<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});

Route::get('/user', function () {
    return view('list');
});

Route::get('/add', function () {
    return view('add');

});

Route::get('/edit', function () {
    return view('edit');

});

Route::resource('people', 'PeopleController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
