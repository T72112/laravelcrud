<?php

use Illuminate\Database\Seeder;

class peopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            [
                'fname'=>'inthira',
                'lname'=>'manorom',
                'age'=> 22
            ],
            [
                'fname'=>'TK',
                'lname'=>'BB',
                'age'=> 26
            ]
        ]);
    }
}
