@extends('layouts.master')
@section('title','list')
@section('css')
   @parent
<link rel="stylesheet" href="{{ asset('css/main.css') }}">
<style>
  
</style>
@endsection
@section('content')

@if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />


  @endif
<div class="col-md-12 text-md-right">
  <a href="add" class= "btn btn-success" id ="add" >ADD</a>
</div>
  <br/>
    <table class="table text-md-center" >
        <thead class="thead-dark">
            <th>ID</th>
            <th>Fname</th>
            <th>Lname</th>
            <th>AGE</th>
            <th>Create Date</th>
            <th>Update Date</th>
            <th>Action Button</th>
            
        </thead>
        <tbody>
            @foreach ($people as $p)
            <tr>
            <td > {{ $p->id }}</td>
            <td>{{ $p->fname}}</td>
            <td>{{ $p->lname}}</td>
            <td>{{ $p->age}}</td>
            <td>{{ date('d-m-Y', strtotime($p->created_at)) }}</td>
            <td>{{ date('d-m-Y', strtotime($p->updated_at)) }}</td>
            <td> 
                <div class="form-inline" >
                {{-- <a href="{{route('people/'.$p->id.'/edit')}}" class="btn btn-primary">Edit</a> --}}
                <div  style="margin-right:20px;">
                <a href="{{ route('people.edit', $p->id) }}" class="btn btn-primary">Edit</a>
            </div>
                <form action="{{ route('people.destroy',[$p->id])}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                      </form>
                    </div>
            </td>
 </div>
            <td>
                   
                </td>
                </tr>
            @endforeach
           
        </tbody>
    </table>
@endsection