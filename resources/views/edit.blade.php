@extends('layouts.master')
@section('title','edit')
@section('content')
<form action="{{ url('people',[$people->id])}}" method="post">
    @csrf
    @method('put')
    <h1>Edit People</h1>
             
    <div class="form-group">
           
        <input id="fname" hidden class="form-control" type="text" name="fname" value="{{ $people->id}}">
   
    <div class="form-group">
        <label for="fname">firstname</label>
    <input id="fname" class="form-control" type="text" name="fname" value="{{ $people->fname}}">
    </div>
    <div class="form-group">
            <label for="lname">lastname</label>
            <input id="lname" class="form-control" type="text" name="lname" value="{{ $people->lname}}">
        </div>
        <div class="form-group">
                <label for="age">age</label>
                <input id="age" class="form-control" type="text" name="age" value="{{ $people->age}}">
            </div>
            <button type="submit" class="btn btn-success">Edit</button>
   @if ($errors->any())
   <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
            <li> {{ $error }}</li>
                @endforeach
            </ul>
   </div>
   @endif
        </form>

@endsection