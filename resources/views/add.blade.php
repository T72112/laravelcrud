@extends('layouts.master')
@section('title','add')
@section('content')
<form action="{{ url('people')}}" method="post">
    @csrf
    <div class="form-group">
        <label for="fname">firstname</label>
        <input id="fname" class="form-control" type="text" name="fname">
    </div>
    <div class="form-group">
            <label for="lname">lastname</label>
            <input id="lname" class="form-control" type="text" name="lname">
        </div>
        <div class="form-group">
                <label for="age">age</label>
                <input id="age" class="form-control" type="text" name="age">
            </div>
            <button type="submit" class="btn btn-success">Save</button>
   @if ($errors->any())
   <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
            <li> {{ $error }}</li>
                @endforeach
            </ul>
   </div>
   @endif
        </form>

@endsection